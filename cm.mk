$(call inherit-product, device/htc/z4dug/z4dug.mk)


# Boot animation
TARGET_SCREEN_WIDTH := 1080
TARGET_SCREEN_HEIGHT := 1920

## Device identifier. This must come after all inclusions

PRODUCT_DEVICE := htc
PRODUCT_NAME := z4dug
PRODUCT_BRAND := HTC
PRODUCT_MANUFACTURER := HTC
PRODUCT_RELEASE_NAME := z4dug
PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME="HTC Desire 500" \
		
    PRIVATE_BUILD_DESC="1.06.401.1 CL223265 release-keys"
BUILD_FINGERPRINT := "htc_europe/z4dug/z4dug:4.1.2/JZO54K/223265.1:user/release-keys"
