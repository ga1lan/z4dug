
# Inherit from those products
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit from z4dug device
$(call inherit-product, device/htc/z4dug/device.mk)

PRODUCT_DEVICE := z4dug
PRODUCT_NAME := z4dug
PRODUCT_BRAND := htc_europe
PRODUCT_MODEL := HTC Desire 500 dual sim
PRODUCT_MANUFACTURER := HTC
