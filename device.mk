#
# Copyright (C) 2013 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

TARGET_PREBUILT_KERNEL := device/htc/z4dug/zImage
PRODUCT_COPY_FILES += \
	$(TARGET_PREBUILT_KERNEL):kernel

#Dalvik Heaps
$(call inherit-product, frameworks/native/build/phone-hdpi-dalvik-heap.mk)
$(call inherit-product, frameworks/native/build/phone-xhdpi-1024-dalvik-heap.mk)

# Bluetooth
PRODUCT_PACKAGES += \
    audio.conf \
    audio_wcn2243.conf \
    auto_pairing.conf \
    blacklist.conf \
    input.conf \
    iop_device_list.conf \
    main.conf \
    network.conf 

# Keylayout
PRODUCT_COPY_FILES += \
    device/htc/z4dug/keylayout/device-keypad.kl:system/usr/keylayout/device-keypad.kl \
    device/htc/z4dug/keylayout/projector-Keypad.kl:system/usr/keylayout/projector-Keypad.kl \
    device/htc/z4dug/keylayout/sr_touchscreen.kl:system/usr/keylayout/sr_touchscreen.kl \
    device/htc/z4dug/keylayout/synaptics-rmi-touchscreen.kl:system/usr/keylayout/synaptics-rmi-touchscreen.kl 

DEVICE_PACKAGE_OVERLAYS += device/htc/z4dug/overlay

#TODO
# The gps config appropriate for this device
$(call inherit-product, device/common/gps/gps_us_supl.mk)

$(call inherit-product-if-exists, vendor/htc/z4dug/z4dug-vendor.mk)

DEVICE_PACKAGE_OVERLAYS += device/htc/z4dug/overlay

$(call inherit-product, build/target/product/full.mk)

PRODUCT_BUILD_PROP_OVERRIDES += BUILD_UTC_DATE=0
PRODUCT_NAME := z4dug
PRODUCT_DEVICE := z4dug
TARGET_KERNEL_SOURCE := kernel/htc/z4u

#TODO
# Ramdisk
PRODUCT_COPY_FILES += \
    device/htc/z4dug/ramdisk/bt_permission.sh:root/bt_permission.sh \
    device/htc/z4dug/ramdisk/cwkeys:root/cwkeys \
    device/htc/z4dug/ramdisk/fstab.z4dug:root/fstab.z4dug \
    device/htc/z4dug/ramdisk/init:root/init \
    device/htc/z4dug/ramdisk/init.qcom.rc:root/init.qcom.rc \
    device/htc/z4dug/ramdisk/init.qcom.sh:root/init.qcom.sh \
    device/htc/z4dug/ramdisk/init.target.rc:root/init.target.rc \
    device/htc/z4dug/ramdisk/init.qcom.usb.rc:root/init.qcom.usb.rc \
    device/htc/z4dug/ramdisk/ueventd.target.rc:root/ueventd.target.rc

#TODO
# ADB
PRODUCT_COPY_FILES += \
    device/htc/z4dug/ramdisk/sbin/adbd:root/sbin/adbd

#TODO
# Set usb type
ADDITIONAL_DEFAULT_PROPERTIES += \
    ro.secure=0 \
    ro.allow.mock.location=0 \
    ro.debuggable=1 \
    persist.service.adb.enable=1 \
    ro.metropcs.ui=0 \
    dalvik.vm.heapstartsize=5m \
    dalvik.vm.heapgrowthlimit=48m \
    dalvik.vm.heapsize=128m \
    dalvik.vm.heaptargetutilization=0.25 \
    dalvik.vm.heapminfree=512k \
    dalvik.vm.heapmaxfree=2m

#TODO
# Telephony
PRODUCT_PROPERTY_OVERRIDES += \
   mobiledata.interfaces=rmnet0 \
  rild.libpath=/system/lib/libhtc_ril.so \
  ro.telephony.ril_class=HtcMsm7x27aRIL \
  ro.telephony.ril.config=datacallapn,signalstrength

#TODO
# Graphics 
PRODUCT_PACKAGES += \
    copybit.msm7x27a \
    gralloc.msm7x27a \
    gralloc.default \
    hwcomposer.msm7x27a \
    libgenlock \
    libtilerenderer \
    liboverlay

# GPS
PRODUCT_PACKAGES += \
    gps.default.so \
    agps_rm\
    gps.conf

# Audio
PRODUCT_PACKAGES += \
    audio.primary.msm7x27a \
    audio.primary.default \
    audio.a2dp.default \
    audio_policy.default.so \
    libaudioutils \
    audio_policy.conf \
    libaudio-resampler

# Audio
PRODUCT_COPY_FILES += \
    device/htc/z4dug/config/audio_policy.conf:system/etc/audio_policy.conf


# Lights
PRODUCT_PACKAGES += \
    lights.msm7x27a


# Power HAL
PRODUCT_PACKAGES += \
    power.msm7x27a
    

#Health HAL
PRODUCT_PACKAGES += \
    libhealthd.msm7x27a

#TODO
# Camera
PRODUCT_PACKAGES += \
    camera.msm7x27a \
    libsurfaceflinger_client

# Newer camera API isn't supported.
PRODUCT_PROPERTY_OVERRIDES += \
   camera2.portability.force_api=1

#TODO
# Video decoding
PRODUCT_PACKAGES += \
    libstagefrighthw \
    libOmxCore \
    libdashplayer

#TODO
# qcmediaplayer
PRODUCT_PACKAGES += \
    qcmediaplayer

# WiFi
PRODUCT_PACKAGES += \
    hostapd \
    wpa_supplicant.conf \
    p2p_supplicant.conf 

#TODO
# Other
PRODUCT_PACKAGES += \
    libnetcmdiface

#TODO
# Hardware properties 
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/native/data/etc/android.hardware.location.xml:system/etc/permissions/android.hardware.location.xml \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
    frameworks/native/data/etc/android.software.sip.xml:system/etc/permissions/android.software.sip.xml \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
    frameworks/native/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml


PRODUCT_AAPT_CONFIG := normal hdpi
PRODUCT_AAPT_PREF_CONFIG := hdpi
